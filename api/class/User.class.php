<?php
	class User
	{
		private $id;
		private $login;
		private $psw;
		private $fullName;
		private $paysRes;
		private $dateNais;

		public function setId($id){
			$this->id = $id;
		}
				
		public function setLogin($login){
			$this->login = $login;
		}
		
		public function setPsw($psw){
			$this->psw = $psw;
		}
		
		public function setFullName($fullName){
			$this->fullName = $fullName;
		}

		public function setPaysRes($paysRes){
			$this->paysRes = $paysRes;
		}

		public function setDateNais($dateNais){
			$this->dateNais = $dateNais;
		}
		
		public function getId(){
			return $this->id;
		}
		public function getLogin(){
			return $this->login;
		}

		public function getPsw(){
			return $this->psw;
		}

		public function getFullName(){
			return $this->fullName;
		}

		public function getDateNais(){
			return $this->dateNais;
		}

		public function getPaysRes(){
			return $this->paysRes;
		}

	}

?>

