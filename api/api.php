
<?php
	include_once('class/User.class.php');

   /***********************Fonction pour les users************************************/
   /**
    * login est une fonction qui permet à a l'utilisateur de s'authentifier
    * @param  base [PDO] connexion a la base de données
    * @param  login [string] login utilisateur
    * @param  pswd [string] mot de passe de l'utilisateur
    * @return [Boolean]
    */
    function login($base, $login, $pswd){

	 	//Requête de selection de l'id du login passer en paramètre
		$query = "SELECT * FROM user WHERE login ='".$login."' AND psw = '".$pswd."';";
		//Execution de la requête
		$reponse = $base->query($query);
		$user_item = [];
		//Récuperation de l'individu connecté
		//$user = [];
	
		while($ligne = $reponse->fetch()){
			$user =  new User();
			$user->setId($ligne['id']);
			$user->setLogin($ligne['login']);
			$user->setFullName($ligne['fullName']);
		}
			//echo json_encode($user);
		return $user;

    }
    /**
     * @param  base [PDO] connexion a la base de données
     * @param  login [string] login utilisateur
     * @param  pswd [string] mot de passe de l'utilisateur
     * @param  dateNais [date] la date de naissance de l'utilisateur
     * @param  paysResi [string] le pays de résidence de l'utilisateur
     * @param  userName [string] le nom et prénom de l'utlisateur
     * @return [Integer]
     */
	function createUser($base, $user){


		 if (isExists($base,"user","login",$user->getLogin()) === 0 ) {

        	//Requête de création de l'utilisateur
		 	$query = "INSERT INTO `user` (`login`, `psw`, `dateNais`, `paysResi`,`fullName`)  VALUES ('".$user->getLogin()."', '".$user->getPsw()."', '".$user->getDateNais()."', '".$user->getPaysRes()."','".$user->getFullName()."') ;";
            //Execution de la requête
            $reponse = $base->exec($query);

            if (isExists($base,"user","login",$user->getLogin()) > 0 ) {

            	//Utilisateur bien créer
                return 0;
            } else {
            	//Probleme lors de la création
               return 1;
            }
        } else {
        	//Utilisateur existe dèjà !!!;
            return 2;
        }


	}
	/**
	 * [getUserByLogin description]
	 * @param  [PDOO] $base [connexion a la base de données]
	 * @param  [string] $login [login utilisateur]
	 * @return [Array]       [l'utilisateur]
	 */

	function getUserByLogin($base, $login){
		$user =  [];

		$query = "SELECT * FROM user  WHERE login ='".$login."' ;";
		//Execution de la requête
		$reponse = $base->query($query);
		while($ligne = $reponse->fetch()){
			$user =  new User();
			$user->setId($ligne['id']);
			$user->setLogin($ligne['login']);
			$user->setFullName($ligne['fullName']);
		}
		return $user ;

	}

 /***********************Fonction pour les messages************************************/

 	/**
 	 * @param  base [PDO] connexion a la base de données
 	 * @param contMess [string] le message écrit à poster
 	 * @param userId [int] id de l'utilisateur qui a envoyer
 	 * @return  [int]
 	 */
	function sendMessage($base, $contMess, $userId){
		//Requête de création de l'utilisateur
		 	$query = "INSERT INTO `messages` (`contMess`, `idUser`)  VALUES ('".$contMess."', '".$userId."') ;";
            //Execution de la requête
            $reponse = $base->exec($query);

           // echo $count ;
            if (isExists($base,"messages","contMess",$contMess) > 0 ) {

            	//Utilisateur bien créer
                return 0;
            } else {
            	//Probleme lors de la création
               return 1;
            }

	}

	/**
  * @param  base [PDO] connexion a la base de données
  * @param  table [string] la table indexer pour la récuperation de données
  * @return reponse [Array] listes des elements de la table
 */
    function getAllItemJoinUser($base, $idMess){
	  	//Requête de selection
    	$query = "SELECT u.id as id, u.fullName as fullName,m.id as messId, m.contMess as mess, m.dateMess as dateMess FROM user u, messages m WHERE u.id = m.idUser AND m.id ".$idMess." ORDER BY m.id DESC LIMIT 8 ;";
		//Execution de la requête
		$reponse = $base->query($query);
		$message_list = [];
		while($ligne = $reponse->fetch()){
			$message = [];
			$message ['messId'] = $ligne['messId'];
			$message ['id'] = $ligne['id'];
			$message['fullName'] = $ligne['fullName'];
			$message['mess'] = $ligne['mess'];
			$message['dateMess'] = $ligne['dateMess'];
			$message_list[] = $message;
		}

		return $message_list;

    }

/****************Fonction génerique ******************************/

/**
	 * La fonction isUserExists permet de vérifier si le login existe dèjà dans la base de données
	 * @param  base [PDO] connexion a la base de données
     * @param  table [string] la table de vérification
     * @param  col [string] la colonne de vérification
     * @param  param [string] la valeur de vérification
	 * @return boolean
	 */
	  function isExists($base, $table, $col, $param) {
	  	//Initialisation du boulean
	  	$isExist = 0;
	  	//Requête de selection de l'id du login passer en paramètre
		$query = "SELECT id FROM ".$table." WHERE ".$col." ='".$param."' ;";
		//Execution de la requête
		$reponse = $base->query($query);
		//Vérification si l'id est vide ou pas
		if( $reponse->fetchColumn() > 0 ) {
		 	 $isExist = 1;
		}
		return $isExist;

    }


/**
  * @param  base [PDO] connexion a la base de données
  * @param  table [string] la table indexer pour la récuperation de données
  * @return reponse [Array] listes des elements de la table
 */
    function getAllItem($base, $table){
	  	//Requête de selection
    	$query = "SELECT * FROM ".$table." ;";
		//Execution de la requête
		$reponse = $base->query($query);

	 	$user_list =  [];

		while($ligne = $reponse->fetch()){
			$user =  new User();
			$user->setId($ligne['id']);
			$user->setLogin($ligne['login']);
			$user->setFullName($ligne['fullName']);
			$user_list[] = $user;
		 }
	    return ($user_list);

	    }

?>
