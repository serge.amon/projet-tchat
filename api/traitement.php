<?php
	require_once 'connect.php';
	include('api.php');

	//Lorsqu'on clique sur le button se connecter
	if(isset($_POST["traitement"]) && $_POST["traitement"] === "auth"){

		/*Récuperation des informations de connexion*/
		$login = $_POST["username"];
		$pswd  = $_POST["password"];

		$result = login($base, $login, $pswd);
		 if( $result->getId() !== null ){
			$user["id"] = $result->getId();	
			$user["login"] = $result->getLogin();	
			$user["fullName"] = $result->getFullName();	
			echo  json_encode($user);
		} else {
		 	echo json_encode(["Echec de l'authentification"]);
		}
	}
	if(isset($_POST["traitement"]) && $_POST["traitement"] === "registBtn"){
		$user = new User();
		$user->setLogin($_POST["userLogin"]);
		$user->setPsw($_POST["userPsw"]);
		$user->setFullName($_POST["userName"]);
		$user->setPaysRes($_POST["userPays"]);		
		$user->setDateNais($_POST["dateNais"]);

		// $login = $_POST["userLogin"];
		// $pswd = $_POST["userPsw"];
		// $dateNais = $_POST["dateNais"];
		// $paysResi = $_POST["userPays"];
		// $userName = $_POST["userName"];
		$result = createUser($base, $user);
		switch ($result) {
			case 0:
			       $message = "Utilisateur bien enregistrer !!! ";
			       $alert   = "success";
				break;
			case 1:
			       $message = "Problème lors de la création  !!! ";
			       $alert   = "warning";
				break;

			default:
			        $message = "Utilisateur existe dèjà !!!";
			        $alert   = "danger";
				break;
		}

		echo json_encode(["message"=>$message,"alert"=>$alert]);
	}
	if(isset($_POST["traitement"]) && $_POST["traitement"] === "sendMessge"){

		$error = false;
		$contMess = $_POST["contMess"];
		$userId = $_POST["userId"];
		$result = sendMessage($base, $contMess, $userId);
		if( $result === 1  ){
			$message ="Echec de l'envoi";
			$error = true;
		}
		echo json_encode(["error"=>$error,"message"=>$message]);

	}
	if(isset($_GET["traitement"]) && $_GET["traitement"] === "userList"){
		$user_list = [];	
		foreach (getAllItem($base,"user") as $user) {
			$user_item["id"] = $user->getId();	
			$user_item["login"] = $user->getLogin();	
			$user_item["fullName"] = $user->getFullName();	
			$user_list [] = $user_item;	
      	}
      	echo json_encode($user_list);
	}
	if(isset($_GET["traitement"]) && $_GET["traitement"] === "messCont"){

		echo json_encode(array_reverse(getAllItemJoinUser($base, $_GET['idMess']))) ;

	}
?>
