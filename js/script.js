//Déclaration des variables
	var userCookie = (document.cookie.replace(/\s/g, '')).split(';');

	/**
	 * [chechAndGetCookies permet de récuperer les cookies]
	 * @param  {[Array]} listCookie [Liste des cookies]
	 * @return {[void]}
	 */
	function chechAndGetCookies(){
		//Parcours de la listes des cookies
		var user =  {};
		for (var i = 0; i < userCookie.length; i++) {
			if(userCookie[i] !==""){
				cookieItem = userCookie[i].split("=");
				user[cookieItem[0]] = cookieItem[1];

			}
		}
		return user;
	}
	/**
	 * [callAjaxSendMessage d'envoyé un message]
	 * @return {[void]} [description]
	 */
	function callAjaxSendMessage(){

		//Récuperation des données du formulaire et réconstitution des paramètres
		parametres = getInputValue("sendMessge",["contMess","userId"]);
		var xhr = new XMLHttpRequest();

		xhr.open('POST','../api/traitement.php',true);
		xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");

		//Ecoute de la réponse de la requête asynchrone
		xhr.addEventListener('load',function(){
			if(! JSON.parse(xhr.responseText).error){
				reseetInputValue(["contMess"]);		
			}else{
					document.getElementById("mmsg-err-send").style.display = "block";
	         		document.getElementById("msg-err-send").innerHTML  ="<h4 id='msg-err' >"+JSON.parse(xhr.responseText)+"</h4>";

	         		setTimeout(function(){
		         		document.getElementById("msg-err-send").style.display = "none";
		         		document.getElementById("msg-err-send").innerHTML = '';
	         		}, 3000)
			
			}
			 		
		})
		xhr.send(parametres);
	}
	/**
	 * [logout description]
	 * @return {[type]} [description]
	 */
	function logout(){

		document.cookie = 'userid=; username=; expires=Mon, 1 Mar 2010 00:00:00 UTC; path=/';
		document.cookie = 'username=; expires=Mon, 1 Mar 2010 00:00:00 UTC; path=/';
		window.location.href="../index.php";
	}

	/**
	 * [getInputValue Permet de réconstituer les parametres pour la requête ajax]
	 * @param  {[String]} btn       [nom de l'action a déclancher dans le fichier php]
	 * @param  {[Array]} inputList [liste des champs des données a envoyer]
	 * @return {[String]}           [une liste de paramètres réconstruire]
	 */
	function getInputValue(btn, inputList){

		params = "traitement="+btn;
		for (var i = 0; i < inputList.length; i++) {

			params +="&"+inputList[i]+"="+document.getElementById(inputList[i]).value;
		}

		return params
	}
	/**
	 * [reseetInputValue permet de vider tot les champs]
	 * @param  {[Array]} inputList [liste des champs des données a envoyer]
	 * @return {[void]}
	 */
	function reseetInputValue(inputList){
		for (var i = 0; i < inputList.length; i++) {

			document.getElementById(inputList[i]).value ='';
		}
	}
	/**
	 * [injectMessage permet d'injecter des messages du tchat]
	 * @return {[void]}
	 */

	function injectMessages(){

		 tchatObject.checkNewMessage();
		//  tchatObject.checkOldMessage();
		// //Récuperation de la position du scrool
		// var messScroll = document.getElementById("messCont").scrollTop;
		// //Vérification si c'est le premier appel ou le scroll n'est plus grand que 0
		// if( firstAjaxCall || messScroll > 0  ){
			
		// 	 firstAjaxCall = false;	
		// }
		
		// else if( ! firstAjaxCall && messScroll === 0  ){
		// }
		//Affichage du contenu de la liste de messages
		tchatObject.display();
		setTimeout(injectMessages, 1000);
	}


	/**
	 * [injectUser permet d'injecter la liste des utilisateurs]
	 * @return {[void]}
	 */
	function injectUsers(){
		/*Vérification de l'existance de la div messCont */;
		if(document.getElementById("userList") !==  null){
			var xhr = new XMLHttpRequest();
			xhr.open('GET','../api/traitement.php?traitement=userList',true);

			//Ecoute de la réponse de la requête asynchrone
			xhr.addEventListener('readystatechange',function(){
				if (xhr.readyState === XMLHttpRequest.DONE){
			
					var userList = "";
					JSON.parse(xhr.responseText).forEach(function(element,item) {
					userList+='<div class="box-comment">'
					+'<img class="img-circle img-sm" src="../img/logo.png" alt="User Image">'
					+'<span style="padding: 15px;">'+element['fullName']+' </span>'
					+' </div>';
					});

					document.getElementById("userList").innerHTML = userList;
				}
			})

			xhr.send(null);
		}
		//Recuperation des utilisateurs chaque 900 ms
		setTimeout(injectUsers,3600);
	}
	/*Vérification de l'existance de la div messCont */;
	if(document.getElementById("messCont") !==  null){
		document.getElementById("messCont").addEventListener("scroll", function(){
			var messScroll = document.getElementById("messCont");
			if(messScroll.scrollTop === 0){
				console.log('r',messScroll.scrollTop)
			
	         		
		         		//document.getElementById("mess-charg").style.display = "none";
					tchatObject.checkOldMessage();				
					tchatObject.display();	
		         		//document.getElementById("mess-charg").innerHTML = '';
		         		//messScroll.scrollTop = (messScroll.scrollHeight/2)-30;
	         		
			}

		});
           
		//Injection des messages

		injectMessages();	
	}

	/*Evenement du clique sur le button connexion*/
	/*Vérification de l'existance du button auth  */
	if(document.getElementById("auth") !== null ){
		user = chechAndGetCookies();
		if( typeof user.userid !== 'undefined' || typeof user.username !== 'undefined' ){
			/*Rédirection vers la page dd'authentification du tchat*/
			window.location.href="html/dash.php"
		}
		document.getElementById("auth").addEventListener('click',function(){
		//Récuperation des données du formulaire
		parametres = getInputValue("auth",["username","password"]);
			// alert(parametres);

		 var xhr = new XMLHttpRequest();
		 xhr.open('POST','api/traitement.php',true);
		 xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");

		 //Ecoute de la réponse de la requête asynchrone
		 xhr.addEventListener('readystatechange',function(){
	         if (xhr.readyState === XMLHttpRequest.DONE){
						// console.log(xhr.responseText)
	         		data = JSON.parse(xhr.responseText);
	         	if( typeof data.login !=='undefined' ){
	         		//Création du cookie
	         		document.cookie = 'userid='+data.id+'; expires=Mon, 1 Mar 2019 00:00:00 UTC; path=/';
	         		document.cookie = 'username='+data.login+'; expires=Mon, 1 Mar 2019 00:00:00 UTC; path=/';

	         		//Connexion Ok, redirection vers le tchat
	         		window.location.href="html/dash.php";

	         	}
	         	else {
	         		//Echec de l'authentification
	         		document.getElementById("msg-err-div").style.display = "block";
	         		document.getElementById("msg-err").innerHTML = data[0];

	         		setTimeout(function(){
		         		document.getElementById("msg-err-div").style.display = "none";
		         		document.getElementById("msg-err").innerHTML = '';
	         		}, 3000);

	         	}

	         }
		})

		xhr.send(parametres);


	    });
	}

	/*Vérification de l'existance du button registBtn */

	if(document.getElementById("registBtn") !== null){

	  	/*Evenement du clique sur le button s'enregistrer*/
		document.getElementById("registBtn").addEventListener('click',function(){

		//Récuperation des données du formulaire et réconstitution des paramètres
		parametres = getInputValue("registBtn",["userLogin","userPsw","dateNais","userPays","userName"]);

		 var xhr = new XMLHttpRequest();

		 xhr.open('POST','../api/traitement.php',true);
		 xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");

		 //Ecoute de la réponse de la requête asynchrone
		 xhr.addEventListener('readystatechange',function(){
	         if (xhr.readyState === XMLHttpRequest.DONE){
	         	 	data = JSON.parse(xhr.responseText);
	         		document.getElementById("msg-err-div-register").style.display = "block";
	         		document.getElementById("msg-err-div-register").className ='alert alert-'+data.alert+' alert-dismissible';
	         		document.getElementById("msg-errRegis").innerHTML = data.message;
	         		reseetInputValue(["userLogin","userPsw","dateNais","userPays","userName"]);
	         		setTimeout(function(){
		         		document.getElementById("msg-err-div-register").style.display = "none";
		         		document.getElementById("msg-errRegis").innerHTML = '';
	         		}, 3000);
	         }
		})

		xhr.send(parametres);
	    })
	}

	/*Vérification de l'existance  du button registBtn */
	if(document.getElementById("sendMessge") !== null){

		user = chechAndGetCookies();
		// console.log(user)
		if( typeof user.userid === 'undefined' || typeof user.username === 'undefined' ){
			/*Rédirection vers la page dd'authentification du tchat*/
			console.log("deconnexion");
			logout();
		}else {

			document.getElementById("userId").value = user.userid ;
			document.getElementById("headUserName").innerText = user.username;
		}
		/*Evenement du clique sur le button envoyer*/
		document.getElementById("sendMessge").addEventListener('click',function(e){
		callAjaxSendMessage();
		injectMessages();
		})
	}
	/*Vérification de l'existance du lien deconnexion */
	if(document.getElementById("logoutId") !== null){
		document.getElementById("logoutId").addEventListener('click',function(){
		logout();

		})
	}

//Chargement des messages, liste des utilisateurs

injectUsers();
console.log(tchatObject)