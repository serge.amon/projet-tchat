<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Tchat | master 2 </title>

  <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="css/AdminLTE.min.css">
   <link rel="stylesheet" href="css/style.css">

</head>

 <body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <b>Tchat</b>
          <div class="login-box-body">
            <p class="login-box-msg">Demarrer votre session</p>
                <div class="alert alert-danger alert-dismissible" id="msg-err-div" style="display:none">
                <h4 id="msg-err" ></h4>
               
              </div>
              <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Login" id="username" >
              </div>
              <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Mot de passe" id="password">
              </div>
              <div class="row">

                <div class="pull-right" style=" margin-top:10px; margin-right: 20px;">
                  <input type="button" class="btn btn-primary btn-block btn-flat" style="background:#dc3996;" value="Connexion" id="auth"/>
              </div>


            <div class="reinitInfo">
    	         <a href="#">Mot de passe oublié</a><br>
    	   		 <a href="html/register.php">S'enregistrer</a>
            </div>


          </div>
        </div>
    </div>

  <script src="js/script.js"></script>
 </body>
</html>
