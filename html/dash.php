<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Tchat | master 2 </title>

  <link rel="stylesheet" href="../css/bootstrap.min.css">
   <link rel="stylesheet" href="../css/AdminLTE.min.css">
   <link rel="stylesheet" href="../css/style.css">

</head>
<body class="hold-transition skin-blue sidebar-mini">
  <header class="main-header" id="head">
    <div class="row">
        <!--div class="head"-->
            <div class="col-md-4" ><img src="../img/lille3"> </div>
            <div class="col-md-8" > 
              <div class="pull-right" style="margin-top: 2%">
               <ul class="list-inline">
                   <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                      <img class="img-circle img-sm" src="../img/logo.png" alt="User Image">  
                      <span class="hidden-xs" style="padding: 10px;" id="headUserName"></span>
                    </a>
                  </li>                  
                  <li class="list-inline-item" > <a ><span id="logoutId">Déconnexion</span></a></li>
                </ul>
              </div>
            </div>
    </div>
  </header>
  <section class="content">
    <div class="row">
      <div class="col-md-3" style="margin-top:2%">
        <div class="box box-solid">
          <div class="box-header with-border">
           <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Recherche">
          </div>
          </div>
           <div class="alert alert-danger alert-dismissible" id="msg-err-send" style="display:none"> </div>
          <div class="box-body no-padding" style=" max-height: 454px; overflow: auto;">
             <div id="userList" > </div>   
          </div>
        </div>
      </div>
      <div class="col-md-9" style="margin-top:2%">
        <div class="box-body">

          <div class="alert alert-info" id="mess-charg" style="display:none">  </div>
          <div class="direct-chat-messages" id="messCont">  </div>
            <div class="box-footer">
              <div class="input-group">
                <input type="text" placeholder="Ecrivez votre Message ..." class="form-control" id="contMess">
                <input type="hidden" id="userId" >
                <span class="input-group-btn">
                <input type="button" class="btn btn-primary btn-block btn-flat" style="background:#dc3996;" value="Envoyer" id="sendMessge"/>
                </span>
              </div>
            </div>
        </div>
      </div>
    </div>
  </section>
<footer >
  <div class="foot">
   <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div> <strong>Copyright © 2018-2019 <a> Serge AMON</a>.</strong> TOUS DROITS RÉSERVÉS.
  </div>
</footer>
<script src="../js/tchatObject.js"></script>
<script src="../js/script.js"></script>
</body>

</html>


  
