<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Tchat | master 2 </title>
  <link rel="stylesheet" href="../css/bootstrap.min.css">
   <link rel="stylesheet" href="../css/AdminLTE.min.css">
  <link rel="stylesheet" href="../css/style.css">

</head>

 <body class="hold-transition login-page">
  <div class="login-box">

      <div class="login-box-body">
        <p class="login-box-msg">Inscription</p>
           <div class="alert alert alert-dismissible" id="msg-err-div-register" style="display:none">
            <h4 id="msg-errRegis" ></h4>
           </div>
         <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Nom & Prénoms" id="userName" required>
          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Login" id="userLogin" required>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Mot de passe" id="userPsw" required>
          </div>

          <div class="form-group has-feedback">
            <input type="date" class="form-control" placeholder="Date naissance" id="dateNais">
          </div>
          <div class="form-group has-feedback">          
            <select class="form-control " style="width: 100%;" id="userPays">
                <option value="" disabled >Pays de résidence </option>
                <option value="France">France</option>
                <option value="Espagne">Espagne</option>
                <option value="Italie">Italie</option>
                <option value="Russie">Russie</option>
                <option value="Pays-bas">Pays-Bas</option>
                <option value="Angleterre">Angleterre</option>
              </select>
          </div>
          <div class="row" >
            <div class="col-xs-6">
                <a href="../index.php" class="text-center">Dèjà un compte</a>
            </div>
            <div class="col-xs-6">
                <div class="pull-right">
                <input id="registBtn" type="button" class="btn btn-primary btn-block btn-flat" style="background:#dc3996" value="S'enregistrer" />
                </div>
            </div>
          </div>
    </div>
  </div>
  <script src="../js/script.js"></script>
 </body>
</html>
